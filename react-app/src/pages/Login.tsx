import axios from "axios";
import React, { useState } from "react";
import { Button } from "react-bootstrap";

export default function Login() {
  const [login, setLogin] = useState<string>();
  const [pass, setPass] = useState<string>();

  return (
    <div className="App">
      <div>
        <label>
          Логин
          <input
            type={"text"}
            value={login}
            onChange={(e) => setLogin(e.target.value)}
          />
        </label>
      </div>
      <div>
        <label>
          Пароль
          <input
            type={"text"}
            value={pass}
            onChange={(e) => setPass(e.target.value)}
          />
        </label>
      </div>
      <div>
        <Button
          variant="primary"
          onClick={() => axios.post("any", { login, pass })}
        >
          login
        </Button>
      </div>
    </div>
  );
}
