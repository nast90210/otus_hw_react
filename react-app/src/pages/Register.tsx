import axios from "axios";
import React, { useState } from "react";
import { Button } from "react-bootstrap";

export default function Register() {
  const [login, setLogin] = useState<string>();
  const [pass, setPass] = useState<string>();
  const [email, setEmail] = useState<string>();

  return (
    <div className="App">
      <div>Регистрация</div>
      <div>
        <label>
          Логин
          <input
            type={"text"}
            value={login}
            onChange={(e) => setLogin(e.target.value)}
          />
        </label>
      </div>
      <div>
        <label>
          Пароль
          <input
            type={"text"}
            value={pass}
            onChange={(e) => setPass(e.target.value)}
          />
        </label>
      </div>
      <div>
        <label>
          Email
          <input
            type={"text"}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </label>
      </div>
      <div>
        <Button
          variant="outline-dark"
          onClick={() => axios.post("any", { login, pass, email })}
        >
          Зарегистрироваться
        </Button>
      </div>
    </div>
  );
}
