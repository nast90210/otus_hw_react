import React from "react";
import { Button, Container, Row } from "react-bootstrap";

import { useAppSelector, useAppDispatch } from "../hooks/hook";
import { decrement, increment } from "../slices/counterSlice";

export default function Home() {
  const count = useAppSelector((state) => state.counterSlice.counter);
  const dispatch = useAppDispatch();
  return (
    <Container>
      <Row>
        <label>Home page</label>
      </Row>
      <Row>
        <a href="/login">Login</a>
      </Row>
      <Row>
        <a href="/register">Registry</a>
      </Row>
      <Row>
        <label>{count}</label>
        <Button variant="primary" onClick={() => dispatch(increment())}>
          INCREMENT
        </Button>
        <Button variant="primary" onClick={() => dispatch(decrement())}>
          DECREMENT
        </Button>
      </Row>
    </Container>
  );
}
